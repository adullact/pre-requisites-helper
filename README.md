# Pre-requisites helper

Help defining pre-requisites for a project regarding end-of-life of components.

## Programming languages and frameworks

* [Java](Java.md)
* [PHP > Symfony](PHP-Symfony.md)
* [Ruby > Ruby on Rails](Ruby-RoR.md)

## Databases

* [MariaDB](DB-Mariadb.md)
* [Mysql](DB-Mysql.md)
* [Postgresql](DB-Postgresql.md)

## OSes

* [Debian](OS-Debian.md)
* [Ubuntu](OS-Ubuntu.md)

