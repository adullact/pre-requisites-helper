# OS Ubuntu end-of-life

* [Ubuntu lifecycle](https://ubuntu.com/about/release-cycle)
* [Ubuntu list of releases (past + future)](https://wiki.ubuntu.com/Releases)

| Ubuntu    | Release date | End of standard support |
|-----------|-------------:|------------------------:|
| ~~16.04~~ |      2016-04 |                 2021-04 | 
| ~~18.04~~ |      2018-04 |                 2023-04 | 
| 20.04     |      2020-04 |                 2025-04 |
| 22.04     |      2022-04 |                 2027-04 |
| 24.04     |      2024-04 |                 2029-04 |

## Resources

* [Ubuntu packages](https://packages.ubuntu.com/)
