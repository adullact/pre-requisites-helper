# Java end-of-life

* [OpenJDK Roadmap](https://adoptopenjdk.net/support.html#roadmap)
* See also [Java version history](https://en.wikipedia.org/wiki/Java_version_history)

| OpenJDK | Released |    End of support |
|---------|---------:|------------------:|
| 8       |  2014-03 | At Least May 2026 |
| 11      |  2018-09 | At Least Oct 2024 |
| 17      |  2021-09 | At Least Oct 2027 |
| 21      |  2023-09 | At Least Oct 2028 |

## OpenJDK in Ubuntu

| Ubuntu        | Shipped OpenJDK | Distro EOL |
|:--------------|----------------:|-----------:|
| 16.04         |               8 |    2021-04 |
| 18.04         |              11 |    2023-04 |
| 20.04         |              11 |    2025-04 |
| 22.04 (jammy) |              17 |    2027-04 |

(See also [Ubuntu EOL](OS-Ubuntu.md))

## OpenJDK in Debian

| Debian                  | Shipped OpenJDK | Distro EOL |
|:------------------------|----------------:|-----------:|
| 9 *Stretch*             |               8 |    2022-06 |
| 10 *Buster*             |              11 |    2024-06 |
| 11 *bullseye*           |              11 |    2026-06 |
| 12 *bookworm*  (stable) |              17 |    2026-06 |

(See also [Debian EOL](OS-Debian.md))
