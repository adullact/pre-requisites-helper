# PHP Symfony 

Map of respective end-of-life (EOL) for releases of Symfony, PHP, Debian, Ubuntu.

## PHP EOL

* [PHP Supported versions](https://www.php.net/supported-versions.php)
* [Migrating from one version of PHP to another version](https://www.php.net/manual/fr/appendices.php)

| PHP version |    Released | Active Support Until | Security Support Until |
|:------------|------------:|---------------------:|-----------------------:|
| ~~7.2~~     | 30 Nov 2017 |          30 Nov 2019 |            30 Nov 2020 |
| ~~7.3~~     |  6 Dec 2018 |           6 Dec 2020 |             6 Dec 2021 |
| ~~7.4~~     | 28 Nov 2019 |          28 Nov 2021 |            28 Nov 2022 |
| ~~8.0~~     | 26 Nov 2020 |          26 Nov 2022 |            26 Nov 2023 |
| 8.1         | 25 Nov 2021 |          25 Nov 2023 |            31 Dec 2025 |
| 8.2         |  8 Dec 2022 |          31 Dec 2024 |            31 Dec 2026 |
| 8.3         | 23 Nov 2023 |          31 Dec 2025 |            31 Dec 2027 |

## Symfony EOL

* [Symfony Releases](https://symfony.com/releases)
* [Symfony pre-requisites](https://symfony.com/doc/master/setup.html#technical-requirements)
* [Backward Compatibility Promise](https://symfony.com/doc/master/contributing/code/bc.html)
* [Release Process](https://symfony.com/doc/master/contributing/community/releases.html)
    * **Patch** version
        * every month
        * only contains bug fixes
    * **Minor** version
        * every six months (one in May and one in November)
        * only  contains bug fixes and new features, but it doesn't include any breaking change)
    * **Major** version
        * every two years
        * contains breaking changes
        * change the necessary version of PHP

| Symfony     | Min PHP version |      Released | End of support | End of bug fixes | End of security fixes |
|:------------|----------------:|--------------:|---------------:|-----------------:|----------------------:|
| ~~4.4 LTS~~ |           7.1.3 | November 2019 |        See --> |    November 2022 |         November 2023 |
| ~~5.0~~     |           7.2.5 | November 2019 |      July 2020 |                  |                       |
| ~~5.1~~     |           7.2.5 |      May 2020 |   January 2021 |                  |                       |
| ~~5.2~~     |           7.2.5 | November 2020 |      July 2021 |                  |                       |
| ~~5.3~~     |           7.2.5 |      May 2021 |   January 2022 |                  |                       |
| 5.4 LTS     |       PHP 7.2.5 | November 2021 |  November 2024 |    November 2024 |         November 2025 |
| ~~6.0~~     |       PHP 8.0.2 | November 2021 |      July 2022 |                  |                       |
| ~~6.1~~     |       PHP 8.0.2 |      May 2022 |   January 2023 |                  |                       |
| ~~6.2~~     |       PHP 8.0.2 | November 2022 |      July 2023 |                  |                       |
| ~~6.3~~     |       PHP 8.0.2 |      May 2023 |   January 2024 |                  |                       |
| 6.4 LTS     |         PHP 8.1 | November 2023 |  November 2026 |    November 2026 |         November 2027 |

`(1)` not release yet

Note on how Symfony manages LTS:

> This deprecation policy also requires a custom development process for major versions (4.0, 5.0, 6.0, etc.) In those cases, Symfony develops at the same time two versions: the new major one (e.g. 5.0) and the latest version of the previous branch (e.g. 4.4).
>
> Both versions have the same new features, but they differ in the deprecated features. The oldest version (4.4 in this example) contains all the deprecated features whereas the new version (5.0 in this example) removes all of them.

## PHP in Ubuntu

* [PHP 7.4 in 20.04](https://discourse.ubuntu.com/t/php-7-4-in-focal/15000)
* [Upcoming PHP 7.4 transition](https://lists.ubuntu.com/archives/ubuntu-devel/2020-February/040901.html)

(See also [Ubuntu EOL](OS-Ubuntu.md))

## PHP in Debian

* [PHP 7.3 in Debian 10 Buster](https://wiki.debian.org/DebianBuster)
* [PHP 7.4 in Debian 11 Bullseye](https://www.debian.org/News/2021/20210814)

(See also [Debian EOL](OS-Debian.md))

## Result of combinations

| Distro family |              Distro version | Supported until | Shipped with PHP | Usable versions of Symfony |
|:--------------|----------------------------:|----------------:|-----------------:|---------------------------:|
| Ubuntu        |                   ~~18.04~~ |         2023-04 |              7.2 |                  4.4 + 5.x |
| Ubuntu        |                       20.04 |         2025-04 |              7.4 |                  4.4 + 5.x |
| Ubuntu        |                       22.04 |         2027-04 |              8.1 |                  5.4 + 6.x |
| Debian        |             ~~9 *Stretch*~~ |           ~2022 |              7.0 |                         NA |
| Debian        | 10 *Buster*  (oldoldstable) |           ~2024 |              7.3 |                  4.4 + 5.x |
| Debian        |   11 *Bullseye* (oldstable) |           ~2026 |              7.4 |                  4.4 + 5.x |
| Debian        |      12 *Bookworm* (stable) |           ~2026 |              8.1 |                  5.x + 6.x |

