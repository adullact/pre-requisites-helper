# OS-Debian

* [Debian Releases official](https://www.debian.org/releases/)
* [Wiki Debian releases Production](https://wiki.debian.org/DebianReleases#Production_Releases)
* [Debian Long Term Support](https://wiki.debian.org/LTS)

| Debian                     | Release date |        EOL |    EOL LTS |
|:---------------------------|-------------:|-----------:|-----------:|
| 9 *Stretch*                |      2017-06 | 2020-07-18 | 2022-07-01 |
| 10 *Buster* (oldoldstable) |      2019-07 | 2022-09-10 | 2024-06-30 |
| 11 *Bullseye*  (oldstable) |      2021-08 |    2024-07 |    2026-06 |
| 12 *Bookworm*  (stable)    |      2023-06 |            |            |


## Resources

* [Debian packages](https://packages.debian.org/)
