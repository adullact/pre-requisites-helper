# Postgresql end-of-life

* [Versioning Policy](https://www.postgresql.org/support/versioning/) --> 5-years support

| Postgresql | Released  | End of support     |
|------------|----------:|-------------------:|
| 9.5        | 2016-01   | February 11, 2021  |
| 9.6        | 2016-09   | November 11, 2021  |
| 10         | 2017-10   | November 10, 2022  |
| 11         | 2018-10   | November 9, 2023   |
| 12         | 2019-10   | November 14, 2024  |

## Postgresql in Ubuntu

| Ubuntu   | Shipped Postgresql | Distro EOL   |
|---------:|-------------------:|-------------:|
| 16.04    | 9.5                | 2021-04      |
| 18.04    | 10                 | 2023-04      |
| 20.04    | 12                 | 2025-04      |

(See also [Ubuntu EOL](OS-Ubuntu.md))

## Postgresql in Debian

| Debian                   | Shipped Postgresql | Distro EOL               | 
|-------------------------:|-------------------:|-------------------------:| 
| 9 *Stretch*  (oldstable) | 9.6                | ~2022                    | 
| 10 *Buster*  (stable)    | 11                 | unknown yet (by 2020-06) | 

(See also [Debian EOL](OS-Debian.md))

