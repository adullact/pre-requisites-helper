# Ruby + Ruby on Rails end-of-life

Map of respective end-of-life (EOL) for releases of Ruby on Rails and Ruby.

## Ruby EOL

* [Ruby Released versions](https://www.ruby-lang.org/en/news/)
* [Upgrade example to another Ruby version](https://help.learn.co/en/articles/2789231-how-to-upgrade-from-ruby-2-3-to-2-6)

| Ruby version | Released     | Active Support Until | Security Support Until |
|--------------|-------------:|---------------------:|-----------------------:|
| 2.4          | 25 Dec 2016  | -----------          | **`31 March 2020`**    |
| 2.5          | 25 Dec 2017  | TBA (Currently)      | TBA                    |
| 2.6          | 25 Dec 2018  | TBA (Currently)      | TBA                    |
| 2.7          | 25 Dec 2019  | TBA (Latest version) | TBA                    |
| 3   `(2)`    | ??.2020      |                      |                        |

`(2)` not release yet

## Ruby on Rails EOL

* [Maintenance Policy of RoR](https://guides.rubyonrails.org/maintenance_policy.html)
* [Upgrading RoR](https://guides.rubyonrails.org/upgrading_ruby_on_rails.html)
* [RoR Releases](https://weblog.rubyonrails.org/releases/)
* [All Rails Versions](https://rubygems.org/gems/rails/versions)
* [Company providing LTS: Rails LTS](https://railslts.com/)

| Ruby on Rails | Min Ruby version | Released       | End of support | End of bug fixes | End of security fixes |
|---------------|-----------------:|---------------:|---------------:|-----------------:|----------------------:|
| 5.0           | 2.2.2            | June 2016      | ??             | ??               | ??                    |
| 6.0           | 2.5.0            | August 2019    | ??             | ??               | ??                    |

