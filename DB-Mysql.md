# DB MySQL end-of-life

## MariaDB / MySQL compatibity

* [MariaDB versus MySQL: Compatibility](https://mariadb.com/kb/en/mariadb-vs-mysql-compatibility/#incompatibilities-between-mariadb-104-and-mysql-80), with details version by version.

# MySQL end-of-life

* [MySQL Lifecycle (EOL)](https://endoflife.software/applications/databases/mysql) on _endoflife.software_ website
* [MySQL 5.7 Community Server End of Life/Support](https://dba.stackexchange.com/questions/234261/mysql-5-7-community-server-end-of-life-support)  on _dba.stackexchange.com_ website

| MySQL | Released | End of support |
|-------|---------:|---------------:|
| 5.6   |     2013 |        2021-02 |
| 5.7   |     2015 |        2023-10 |
| 8.0   |     2018 |        2026-04 |

## MySQL in Ubuntu

| Ubuntu   | Shipped MySQL | Distro EOL |
|----------|--------------:|-----------:|
| 16.04    |         5.7 ? |    2021-04 |
| 18.04    |           5.7 |    2023-04 |
| 20.04    |           8.0 |    2025-04 |


See also:
- [Ubuntu EOL](OS-Ubuntu.md)
- [Ubuntu `mysql-server` packages](https://packages.ubuntu.com/search?keywords=mysql-server&searchon=names)

## MySQL in Debian

| Debian                      | Shipped MySQL | Distro EOL |
|-----------------------------|--------------:|-----------:|
| 9 *Stretch*  (oldoldstable) |           5.5 |    2022-06 |
| 10 *Buster*  (oldstable)    |           5.8 |    2024-06 |
| 11 *bullseye* (stable)      |           5.8 |    2026-06 |


See also:
- [Debian EOL](OS-Debian.md)
- [Debian `mysql-server` packages](https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords=mysql-server)


## MySQL in CentOS

TODO

(See also [CentOS EOL](OS-Centos.md))



