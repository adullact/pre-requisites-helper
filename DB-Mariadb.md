# MariaDB end-of-life

* [Maintenance policy](https://mariadb.org/about/#maintenance-policy) --> 5-years support

| MariaDB |     Released |  End of support |
|---------|-------------:|----------------:|
| 10.1    |  17 Oct 2015 |     17 Oct 2020 |
| 10.2    |  23 May 2017 |     23 May 2022 |
| 10.3    |  25 May 2018 |     25 May 2023 |
| 10.4    | 18 June 2019 |    18 June 2024 |
| 10.5    | 24 June 2020 |    24 June 2025 |
| 10.6    |  6 July 2021 |     6 July 2026 |

## MariaDB in Ubuntu

| Ubuntu   |   Shipped MariaDB |   Distro EOL |
|----------|------------------:|-------------:|
| 16.04    |              10.0 |      2021-04 |
| 18.04    |              10.1 |      2023-04 |
| 20.04    |              10.3 |      2025-04 |

See also:

- [Ubuntu EOL](OS-Ubuntu.md)
- [Ubuntu `mariadb-server` packages](https://packages.ubuntu.com/search?keywords=mariadb-server&searchon=names)

## MariaDB in Debian

| Debian                      |   Shipped MariaDB | Distro EOL |
|-----------------------------|------------------:|-----------:|
| 9 *Stretch*  (oldoldstable) |              10.1 |    2022-06 |
| 10 *Buster*  (oldstable)    |              10.3 |    2024-06 |
| 11 *bullseye* (stable)      |              10.5 |    2026-06 |

See also:

- [Debian EOL](OS-Debian.md)
- [Debian `mariadb-server` packages](https://packages.debian.org/search?searchon=names&keywords=mariadb-server)

## MariaDB in CentOS

| CentOS |  Shipped MariaDB |  Distro EOL |
|--------|-----------------:|------------:|
| 7      |              5.5 |     2024-06 | 
| 8      |             10.3 |     2029-05 | 

(See also [CentOS EOL](OS-Centos.md))

## MariaDB / Mysql compatibility

* [MariaDB versus MySQL: Compatibility](https://mariadb.com/kb/en/mariadb-vs-mysql-compatibility/#incompatibilities-between-mariadb-104-and-mysql-80)
  , with details version by version.
