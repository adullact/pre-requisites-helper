# Centos end-of-life

* [CentOS Product Specifications](https://wiki.centos.org/About/Product)

| CentOS | Full Updates | Maintenance Updates |   
|--------|-------------:|--------------------:|   
| 7      | 2020-Q4      | 2024-06             |   
| 8      | 2024-06      | 2029-05             |   

## Resources

* https://centos.pkgs.org/ 
